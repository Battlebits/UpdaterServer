package br.com.battlebits.updater;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.channels.ClosedByInterruptException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import org.json.JSONObject;

public class UpdaterServer implements Runnable {

	public static void main(String[] args) {
		try {
			UpdaterServer server = new UpdaterServer();
			new Thread(server).start();
			new Thread(new Runnable() {
				@Override
				public void run() {
					Scanner scan = new Scanner(System.in);
					while (scan.hasNextLine()) {
						String line = scan.nextLine();
						if (line.equalsIgnoreCase("stop")) {
							try {
								server.stop();
								break;
							} catch (IOException e) {
								e.printStackTrace();
							}
						} else {
							System.out.println("Para parar o servidor digite: stop");
						}
					}
					scan.close();
				}
			}).start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private ServerSocket server;
	private boolean running;
	private JSONObject config;

	public UpdaterServer() throws IOException {
		server = new ServerSocket(63973);
		running = true;
		FileInputStream fis = new FileInputStream("./config.json");
		BufferedReader br = new BufferedReader(new InputStreamReader(fis));
		StringBuilder builder = new StringBuilder();
		String sCurrentLine;
		while ((sCurrentLine = br.readLine()) != null) {
			builder.append(sCurrentLine);
		}
		br.close();
		fis.close();
		config = new JSONObject(builder.toString());
	}

	@Override
	public void run() {
		System.out.println("Running");
		while (running) {

			try {
				Socket socket = server.accept();
				new Thread(new Runnable() {

					@Override
					public void run() {
						FileInputStream fileInput = null;
						BufferedInputStream bufferedInput = null;
						OutputStream output = null;
						InputStream input = null;
						DataInputStream dataInput = null;
						DataOutputStream dataOutput = null;
						try {
							output = socket.getOutputStream();
							input = socket.getInputStream();
							dataInput = new DataInputStream(input);
							dataOutput = new DataOutputStream(output);
							String user = dataInput.readUTF();

							if (!config.has(user)) {
								dataOutput.writeUTF("DENIED");
								dataOutput.flush();
								throw new ClosedByInterruptException();
							} else {
								dataOutput.writeUTF("OK");
								dataOutput.flush();
							}
							String passwordSha = dataInput.readUTF();
							if (!config.getJSONObject(user).getString("password").equals(passwordSha)) {
								dataOutput.writeUTF("DENIED");
								dataOutput.flush();
								throw new ClosedByInterruptException();
							} else {
								dataOutput.writeUTF("OK");
								dataOutput.flush();
							}
							String version = dataInput.readUTF();

							File folder = new File("./plugins/" + user);

							File[] serverVersions = folder.listFiles();

							List<String> versionList = new ArrayList<>();

							for (File file : serverVersions) {
								versionList.add(file.getName());
							}

							Collections.sort(versionList, new Comparator<String>() {

								@Override
								public int compare(String o1, String o2) {
									String[] levels1 = o1.split("\\.");
									String[] levels2 = o2.split("\\.");

									int length = Math.max(levels1.length, levels2.length);
									for (int i = 0; i < length; i++) {
										Integer v1 = i < levels1.length ? Integer.parseInt(levels1[i]) : 0;
										Integer v2 = i < levels2.length ? Integer.parseInt(levels2[i]) : 0;
										int compare = v1.compareTo(v2);
										if (compare != 0) {
											return -compare;
										}
									}

									return 0;
								}
							});
							File file = new File("./plugins/" + user + "/" + versionList.get(0) + "/" + user + ".jar");
							if (versionList.get(0).equals(version) || !file.exists()) {
								dataOutput.writeUTF("UPDATED");
								dataOutput.flush();
								return;
							}
							dataOutput.writeUTF("NEW");
							dataOutput.flush();

							byte[] byteArray = new byte[(int) file.length()];
							fileInput = new FileInputStream(file);
							bufferedInput = new BufferedInputStream(fileInput);
							bufferedInput.read(byteArray, 0, byteArray.length);
							System.out.println("Enviando " + file.getName() + "(" + byteArray.length + " bytes)");
							output.write(byteArray, 0, byteArray.length);
							output.flush();
							System.out.println("Fim.");
						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							try {
								if (socket != null)
									socket.close();
								if (fileInput != null)
									fileInput.close();
								if (bufferedInput != null)
									bufferedInput.close();
								if (output != null)
									output.close();
								if (input != null)
									input.close();
								if (dataInput != null)
									dataInput.close();
								if (dataOutput != null)
									dataOutput.close();
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}).start();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}

	public void stop() throws IOException {
		running = false;
		server.close();
	}

}
